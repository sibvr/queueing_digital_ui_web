import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CallServiceFunction } from '../callServiceFunction';

@Component({
  selector: 'app-navtop',
  templateUrl: './navtop.component.html',
  styleUrls: ['./navtop.component.css']
})
export class NavtopComponent implements OnInit {

  constructor(private callService: CallServiceFunction,public router:Router) { }

  //service
  serviceReturn : any;
  statusService :any;
  errMess: any;

  logoutDialog(){
    document.getElementById('confirm').style.visibility="visible";
    document.getElementById('confirm').style.opacity="1";
  }

  close() {
    document.getElementById('confirm').style.visibility="hidden";
    document.getElementById('confirm').style.opacity="0";
  }

  logout() {
      let body = {
        "empty": true,
        "additionalProp1": {},
        "additionalProp2": {},
        "additionalProp3": {},
        "url": "logoutService"
      }
  
      this.callService.postRestResultAuthorized(body).subscribe((res)=>{
        console.log(res);
        this.serviceReturn = res;
        this.statusService = this.serviceReturn["0"].serviceContent.status;
  
        if(this.statusService == "S"){
          console.log("status: " + this.statusService);
          sessionStorage.setItem("userId", "");
          sessionStorage.setItem("access-token", "");
          this.router.navigateByUrl('/');
        }
        else{
          console.log("status: " + this.statusService);
          console.log("errorMess: " + this.serviceReturn["0"].serviceContent.errorMessage); 
        }
      }), err => {console.log(err)};
  }

  ngOnInit() {
  }

}
