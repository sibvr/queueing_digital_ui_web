import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CallServiceFunction } from '../callServiceFunction';
import { FormControl, FormGroup,Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private callService: CallServiceFunction,public router:Router, private spinner:NgxSpinnerService) { }

  //service
  serviceReturn : any;
  statusService :any;
  errMess: any;

  //form
  loginForm = new FormGroup({
    userId: new FormControl('',[
      Validators.required
    ]),
    password: new FormControl('',[
      Validators.required
    ])
  })

  showAlert(text){
    document.getElementById("alert-message").innerHTML = `<i class="far fa-times-circle"></i><strong>Login Gagal</strong>${text}`;
    document.getElementById("alert").style.display='block';
    setTimeout(()=>{
      document.getElementById("alert").style.display='none';
    }, 4000);
  }

  login() {

    if(this.loginForm.status != "INVALID"){
      this.spinner.show();
      let body = {
        "username": this.loginForm.controls['userId'].value,
        "password": this.loginForm.controls['password'].value,
        "url":"loginService",
      }
      
      this.callService.postRestResult(body).subscribe((res)=>{
        console.log(res);
        this.serviceReturn = res;
        this.statusService = this.serviceReturn["0"].serviceContent.status;
  
        if(this.statusService == "S"){
          console.log("status: " + this.statusService);
          sessionStorage.setItem("userId", this.loginForm.controls['userId'].value);
          sessionStorage.setItem("access-token", this.serviceReturn["0"].serviceContent.access_token);
          setTimeout(()=>{
            this.spinner.hide();
            this.router.navigateByUrl('/home');
          }, 1500);
          
        }
        else{
          this.errMess = this.serviceReturn["0"].serviceContent.errorMessage;
          this.showAlert(this.errMess);
          setTimeout(()=>{
            this.spinner.hide();
          }, 1000);
          console.log("status: " + this.statusService);
        }
      }), err => {console.log(err)};
    }
    else{
      this.loginForm.controls['userId'].markAsTouched();
      this.loginForm.controls['password'].markAsTouched();
    }
  }

  ngOnInit() {
  }

}
