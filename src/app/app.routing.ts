import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NavtopComponent } from './navtop/navtop.component';
import { QuestionmanagementComponent } from './questionmanagement/questionmanagement.component';


const AppRoutes: Routes = [
    {path:'',component:LoginComponent,outlet:'login'},
    { path: 'home', children:[
        {path:'',component : HomeComponent},
        {path: '' , component: NavtopComponent, outlet: 'navtop'}
    ]},
    { path: 'questionmanagement', children:[
        {path:'',component : QuestionmanagementComponent},
        {path: '' , component: NavtopComponent, outlet: 'navtop'}
    ]},
];

@NgModule({
    imports: [
    RouterModule.forRoot(AppRoutes, {
        // onSameUrlNavigation: 'ignore',
    onSameUrlNavigation: 'reload'
    })
        
    ],
    exports: [RouterModule]
})

export class AppRouting{};

export const RoutedComponents = [
    LoginComponent,
    HomeComponent,
    NavtopComponent,
]
