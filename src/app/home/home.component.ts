import { Component, OnInit } from '@angular/core';
import { CallServiceFunction } from '../callServiceFunction';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public router: Router, private callService: CallServiceFunction,private spinner:NgxSpinnerService) { }

  //service
  serviceReturn: any;
  statusService: any;
  errMess: any;

  //data
  currentQueue: string;
  totalQueue: string;
  type: string;

  //nomor loket form
  inputLoketForm = new FormGroup({
    inputNomorLoket: new FormControl('null', [
      Validators.required
    ]),
  })
  
  buttonChange(type) {
    this.spinner.show();
    this.type = type;
    if (type == "cs") {
      document.getElementById("type").innerHTML = `Antrian ${type.charAt(0).toUpperCase() + type.slice(1)}`;
      document.getElementById(this.type).style.backgroundColor = "var(--dark-purple)";
      document.getElementById(this.type).style.color = "white";
      document.getElementById("type").innerHTML = `Antrian ${type.charAt(0).toUpperCase() + type.slice(1)}`;
      document.getElementById("teller").style.backgroundColor = "white";
      document.getElementById("teller").style.color = "var(--dark-purple)";
    }
    else {
      document.getElementById("type").innerHTML = `Antrian ${type.charAt(0).toUpperCase() + type.slice(1)}`;
      document.getElementById(this.type).style.backgroundColor = "var(--dark-purple)";
      document.getElementById(this.type).style.color = "white";
      document.getElementById("type").innerHTML = `Antrian ${type.charAt(0).toUpperCase() + type.slice(1)}`;
      document.getElementById("cs").style.backgroundColor = "white";
      document.getElementById("cs").style.color = "var(--dark-purple)";
    }
    this.getQueue();
  }

  counterQueue=[];
  getQueue() {
    
    document.getElementById(this.type).classList.add("checked");
    this.spinner.show();

    let body = {
      "parameter": "?type=" + this.type,
      "url": "getCurrentQueueNumberActiveService"
    }

    this.callService.getRestResultAuthorized(body).subscribe((res) => {
      let serviceReturn = res;
      this.statusService = serviceReturn["0"].serviceContent.status;

      if (this.statusService == "S") {
        console.log("status: " + this.statusService);
        this.currentQueue = serviceReturn["0"].serviceContent.currentQueueNumber;
        this.totalQueue = serviceReturn["0"].serviceContent.totalQueueNumber;
        this.counterQueue = serviceReturn["0"].serviceContent.counterQueue;
        document.getElementById("currentQueue").innerHTML = this.currentQueue;
        document.getElementById("totalQueue").innerHTML = this.totalQueue;
        this.getChartData();
        setTimeout(()=>{
          this.spinner.hide();
        }, 1500);
      }
      else {
        console.log("status: " + this.statusService);
        setTimeout(()=>{
          this.spinner.hide();
        }, 1500);
      }
    }), err => { console.log(err) };
  }

  consumeQueue() {

    let userId = sessionStorage.getItem("userId");
    console.log("no counter" + this.inputLoketForm.get("inputNomorLoket").value )
    let body = {
      "userId": userId,
      "type": this.type,
      "counter":this.inputLoketForm.get("inputNomorLoket").value,
      "url": "consumeQueueNumberTellerService"
    }

    console.log("body consume " + body.toString())
    this.callService.putRestResultAuthorized(body).subscribe((res) => {
      let serviceReturn = res;
      this.statusService = serviceReturn["0"].serviceContent.status;
      if (this.statusService == "S") {
        console.log("status: " + this.statusService);
        this.getQueue();
      }
      else {
        console.log("status: " + this.statusService);
      }
    }), err => { console.log(err) };
  }


  totalDaily: any;
  dateQueue: any;
  dataQueue: any[]=[];

  getChartData(){

    // this.totalDaily=[];
    // this.dateQueue=[];
    // this.dataQueue=[];

    this.type = "teller";
    let body = {
      "parameter": "?type=" + this.type,
      "url": "getTotalQueueNumberHistoryService"
    }

    this.callService.getRestResultAuthorized(body).subscribe((res) => {
      let serviceReturn = res;
      this.statusService = serviceReturn["0"].serviceContent.status;

      if (this.statusService == "S") {
        console.log("status: " + this.statusService);
        console.log("len " + serviceReturn["0"].serviceContent.graphList.length);

        for (let i=serviceReturn["0"].serviceContent.graphList.length - 1; i>=0; i--)
        {
          this.totalDaily = serviceReturn["0"].serviceContent.graphList[i].total;
          this.dateQueue = serviceReturn["0"].serviceContent.graphList[i].dateQueue;
          this.dataQueue.push({name: this.dateQueue, value: this.totalDaily});           
        }
       
        this.showGraph="true";

        // setTimeout(()=>{
        //   this.spinner.hide();
        // }, 1500);
      }
      else {
        console.log("status: " + this.statusService);
        setTimeout(()=>{
          this.spinner.hide();
        }, 1500);
      }
    }), err => { console.log(err) };

  }

  openModalCounter(){
    document.getElementById('popup-modal-counter').style.visibility="visible";
    document.getElementById('popup-modal-counter').style.opacity="1";
  }

  closeModalCounter() {
    document.getElementById('popup-modal-counter').style.visibility="hidden";
    document.getElementById('popup-modal-counter').style.opacity="0";
  }

  showGraph: String;
  ngOnInit() {
    this.buttonChange("teller");
    this.showGraph="false";
  }
}




