import { HttpClient } from '@angular/common/http'; 
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class CallServiceFunction{
    constructor(public http: HttpClient){ }

	postRestResult(jsonInput){
        let apiName = jsonInput.url;
        let headers = {"Content-type": "application/json"}
		let body = JSON.stringify(jsonInput);
	    return this.http.post<any>('http://localhost:8881/bvnextgen/api/' + apiName, body, {headers});
    }
    
    getRestResultAuthorized(jsonInput): Observable<any> {
        let apiName = jsonInput.url;
        let parameter = jsonInput.parameter;
        let accessToken = sessionStorage.getItem("access-token")
        let headers = {"Content-type": "application/json","Authorization": "Bearer " + accessToken}
        return this.http.get<any>('http://localhost:8881/bvnextgen/api/' + apiName + parameter, {headers});
    }

    postRestResultAuthorized(jsonInput){
        let apiName = jsonInput.url;
        let accessToken = sessionStorage.getItem("access-token")
        let headers = {"Content-type": "application/json","Authorization": "Bearer " + accessToken}
		let body = JSON.stringify(jsonInput);
	    return this.http.post<any>('http://localhost:8881/bvnextgen/api/' + apiName, body, {headers});
    }

    putRestResultAuthorized(jsonInput){
        let apiName = jsonInput.url;
        let accessToken = sessionStorage.getItem("access-token")
        let headers = {"Content-type": "application/json","Authorization": "Bearer " + accessToken}
		let body = JSON.stringify(jsonInput);
	    return this.http.put<any>('http://localhost:8881/bvnextgen/api/' + apiName, body, {headers});
    }
    
}