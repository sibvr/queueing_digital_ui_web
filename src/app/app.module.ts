import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule} from 'ngx-spinner';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRouting, RoutedComponents} from './app.routing';
import { AppComponent } from './app.component';
import { CallServiceFunction } from './callServiceFunction';
import { NgxChartsModule }from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QuestionmanagementComponent } from './questionmanagement/questionmanagement.component';

@NgModule({
  declarations: [
    AppComponent,
    RoutedComponents,
    QuestionmanagementComponent,
  ],
  imports: [
    BrowserModule,
    AppRouting,
    HttpClientModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    NgxChartsModule,
  ],
  providers: [CallServiceFunction],
  bootstrap: [AppComponent]
})
export class AppModule { }
