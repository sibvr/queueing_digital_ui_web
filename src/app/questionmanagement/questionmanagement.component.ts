import { Component, OnInit } from '@angular/core';
import { CallServiceFunction } from '../callServiceFunction';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ResourceLoader } from '@angular/compiler';
import { Router } from "@angular/router";

@Component({
  selector: 'app-questionmanagement',
  templateUrl: './questionmanagement.component.html',
  styleUrls: ['./questionmanagement.component.css'],
})


export class QuestionmanagementComponent implements OnInit {

  constructor(private callService: CallServiceFunction, public router:Router,  ) { }

  createQuestionFG = new FormGroup({
    inputQuestion: new FormControl('', [
      Validators.required
    ]),
    inputQuestionDestination: new FormControl('null', [
      Validators.required
    ]),
  })

  editQuestionFG = new FormGroup({
    editQuestion: new FormControl('', [
      Validators.required
    ]),
    editQuestionDestination: new FormControl('null', [
      Validators.required
    ]),
  })

  userID: string;
  indexEdit: any;
  indexDelete: any;

  queueingDestination=[];
  question=[];
  questionListInquiry=[];
  questionId=[];

  getQuestionList(){

   
    this.queueingDestination=[];
    this.question=[];
    this.questionId=[];
    this.questionListInquiry=[];
    

    this.userID = sessionStorage.getItem("userId");
    console.log("user id " + this.userID);
    console.log("token " + sessionStorage.getItem("access-token"))

    let body = {
      "userId":this.userID,
      "parameter":"?userId=" + this.userID,
      "url": "getQuestionHelpService"
    }

    
    this.callService.getRestResultAuthorized(body).subscribe((res) => {
      let serviceReturn = res;
      let statusService = serviceReturn["0"].serviceContent.status;

      console.log("status: " + statusService);
      if (statusService == "S") {
        console.log("len " + serviceReturn["0"].serviceContent.questionList.length);

        for (let i=0; i<serviceReturn["0"].serviceContent.questionList.length; i++)
        {
          this.queueingDestination = serviceReturn["0"].serviceContent.questionList[i].queueingDestination;        
          this.question = serviceReturn["0"].serviceContent.questionList[i].question;
          this.questionListInquiry.push(serviceReturn["0"].serviceContent.questionList[i]);
          this.questionId = [i];      
        }

        console.log("question inquiry " + this.questionListInquiry)
    
      }
      else {
        console.log("status: " + statusService);
      }
    }), err => { console.log(err) };

  }


  questionID: any;

  deleteQuestion(){

    console.log("qID " + this.questionID)
    
    this.userID = sessionStorage.getItem("userId");
    console.log("user id " + this.userID);
    console.log("token " + sessionStorage.getItem("access-token"))

    let body = {
      "userId":this.userID,
      "questionId":this.indexDelete,
      "url": "deleteQuestionHelpService"
    }

    this.callService.postRestResultAuthorized(body).subscribe((res) => {
      let serviceReturn = res;
      let statusService = serviceReturn["0"].serviceContent.status;

      console.log("status: " + statusService);
      if (statusService == "S") {

       alert("DELETED");
      //  this.router.navigateByUrl("/questionmanagement");
      this.ngOnInit();
      this.closeModalDelete();
        
      }
      else {
        console.log("status: " + statusService);
      }
    }), err => { console.log(err) };
  }

  createQuestion(){

    this.userID = sessionStorage.getItem("userId");
    let inQuestion = this.createQuestionFG.controls['inputQuestion'].value;
    let inQuestionDestination = this.createQuestionFG.controls['inputQuestionDestination'].value;

    let body = {
      "question": inQuestion,
      "userId": this.userID,
      "questionDestination": inQuestionDestination,
      "url": "createQuestionHelpService"
    }


    this.callService.postRestResultAuthorized(body).subscribe((res) => {
      let serviceReturn = res;
      let statusService = serviceReturn["0"].serviceContent.status;

      console.log("status create: " + statusService);
      if (statusService == "S") {

       alert("CREATED");  
      //  this.router.navigateByUrl('questionmanagement');
      this.ngOnInit();
      this.closeModalCreate();
       
      }
      else {
        console.log("status: " + statusService);
      }
    }), err => { console.log(err) };
  }


  editQuestion(){

    this.userID = sessionStorage.getItem("userId");
    let edQuestion = this.editQuestionFG.controls['editQuestion'].value;
    let edQuestionDestination = this.editQuestionFG.controls['editQuestionDestination'].value;

    let body = {
      "questionId": this.indexEdit,
      "question": edQuestion,
      "queueingDestination": edQuestionDestination,
      "userId": this.userID,
      "url": "editQuestionHelpService"
    }

    this.callService.putRestResultAuthorized(body).subscribe((res) => {
      let serviceReturn = res;
      let statusService = serviceReturn["0"].serviceContent.status;

      console.log("status create: " + statusService);
      if (statusService == "S") {

       alert("EDITED");     
      //  this.router.navigateByUrl("/questionmanagement");
       this.ngOnInit();
       this.closeModalEdit();
       
         
      }
      else {
        console.log("status: " + statusService);
      }
    }), err => { console.log(err) };
  }

  openModalCreate(){
    document.getElementById('popup-modal').style.visibility="visible";
    document.getElementById('popup-modal').style.opacity="1";
  }

  closeModalCreate() {
    document.getElementById('popup-modal').style.visibility="hidden";
    document.getElementById('popup-modal').style.opacity="0";
  }

  openModalEdit(indexEd){
    this.indexEdit=indexEd;
    console.log("index edit " + this.indexEdit);

    console.log("TOTAL DATA " + this.questionListInquiry.length)
    for (var i = 0; i < this.questionListInquiry.length; ++i) {
      if(this.questionListInquiry[i].questionId==this.indexEdit){
        this.editQuestionFG.get("editQuestion").setValue(this.questionListInquiry[i].question);
        this.editQuestionFG.get("editQuestionDestination").setValue(this.questionListInquiry[i].queueingDestination);
      }
    }

    document.getElementById('popup-modal-edit').style.visibility="visible";
    document.getElementById('popup-modal-edit').style.opacity="1";
    
  }

  closeModalEdit() {
    document.getElementById('popup-modal-edit').style.visibility="hidden";
    document.getElementById('popup-modal-edit').style.opacity="0";
  }

  openModalDelete(indexEd){
    this.indexDelete=indexEd;
    console.log("index edit " + this.indexDelete);
    document.getElementById('popup-modal-del').style.visibility="visible";
    document.getElementById('popup-modal-del').style.opacity="1";
  }

  closeModalDelete() {
    document.getElementById('popup-modal-del').style.visibility="hidden";
    document.getElementById('popup-modal-del').style.opacity="0";
  }
  
  ngOnInit() {
  
    this.getQuestionList();
  }

}
